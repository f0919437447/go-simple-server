package app

import (
	"main/src/route"
	"main/src/utils"

	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
)

type app uint

var Server app = 0

func (c *app) Run(server *gin.Engine, nc *nats.Conn) {
	if server == nil {
		server = gin.Default()
	}

	c.Register(server)
	route.Register(server, nc)

	err := server.Run(":8080")
	if err != nil {
		panic(err)
	}
}

func (c *app) Register(server *gin.Engine) {
	// middleware
	server.Use(ConcurrencyLimit(utils.ConcurrencyLimit))
}
