package session

import (
	"main/src/utils"
	"time"

	"github.com/gin-gonic/gin"
)

func SessionBuilder(ctx *gin.Context) {
	session := GetInstence(ctx)

	if session == nil {
		session = NewSession(ctx, utils.SessionKey, Expire{session: time.Hour, cookie: time.Hour})

		session.HandleCookie()
		ctx.Set(utils.SessionKey, session)
	}

	defer session.Save()

	ctx.Next()
}
