package route

import (
	"main/src/auth"
	"main/src/controller"
	"main/src/data"
	"main/src/session"

	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
)

func Register(server *gin.Engine, nc *nats.Conn) {
	// middleware
	api := server.Group(data.API_ROOT)
	api.Use(auth.CheckContext)
	api.Use(session.SessionBuilder)

	// path
	controller.User.Route(api)
	controller.User.NatsRoute(nc)
}
