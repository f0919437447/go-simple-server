package database

import (
	"context"
	"errors"
	"fmt"
	"log"
	"main/src/data"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type dbMaker uint

var (
	DBMaker dbMaker = 0

	Mysql *gorm.DB
	Mongo *mongo.Client
	Redis *redis.Client
)

func (c *dbMaker) Connect() {
	var err error

	Mysql, err = OpenSqlDb()
	if err != nil {
		log.Fatal("[Mysql] connecting error:", err)
	}
	Mongo, err = OpenMongoDb()
	if err != nil {
		log.Fatal("[Mongo] connecting error:", err)
	}
	Redis, err = OpenRedisDb()
	if err != nil {
		log.Fatal("[Redis] connecting error:", err)
	}

	log.Println("[Connection] completed.")
}

func OpenSqlDb() (*gorm.DB, error) {
	settings := getDBConfig("mysql")

	// "user:passwd@tcp(ip:port)/database?..."
	dsnFmt := "%v:%v@tcp(%v:%v)/main?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := fmt.Sprintf(dsnFmt, settings.User, settings.Password, settings.Hostname, settings.Port)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
	})

	if err != nil {
		return nil, errors.New("failed to connect database")
		// panic(err)
	}

	log.Println("[SQL] open db successed.")
	return db, nil
}

func OpenMongoDb() (*mongo.Client, error) {
	settings := getDBConfig("mongo")

	// "mongodb://user:passwd@ip:port/database?..."
	dsnFmt := "mongodb://%v:%v@%v:%v/main?authSource=admin&maxPoolSize=100&maxIdleTimeMS=0"
	dsn := fmt.Sprintf(dsnFmt, settings.User, settings.Password, settings.Hostname, settings.Port)
	op := options.Client().ApplyURI(dsn)

	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(50*time.Second))
	// Even though ctx will be expired, it is good practice to call its
	// cancellation function in any case. Failure to do so may keep the
	// context and its parent alive longer than necessary.
	defer cancel()

	client, err := mongo.Connect(ctx, op)

	if err != nil {
		return nil, err
		// panic(err)
	}

	// rw test
	type info struct {
		Id  int `json:"_id,omitempty" bson:"_id,omitempty"`
		Val int `json:"val,omitempty" bson:"val,omitempty"`
	}
	infoIn := &info{Id: 5, Val: 5}
	coll := client.Database("main").Collection("TEST")
	insertRes, err := coll.InsertOne(ctx, infoIn)
	if err != nil {
		return nil, err
	}
	_ = insertRes
	findRes := coll.FindOne(ctx, nil)

	var infoRs info
	findRes.Decode(&infoRs)
	log.Println(infoRs)

	log.Println("[Mongo] open db successed.")
	return client, nil
}

func OpenRedisDb() (*redis.Client, error) {
	settings := getDBConfig("redis")

	// redis.NewClusterClient(&redis.ClusterOptions{})
	address := fmt.Sprintf("%v:%v", settings.Hostname, settings.Port)
	client := redis.NewClient(&redis.Options{
		Addr:       address,
		Password:   "",
		Username:   "",
		DB:         0,
		MaxConnAge: 0,
		// PoolSize: 10,
		// MinIdleConns: 10,
	})

	pong, err := client.Ping(context.Background()).Result()

	if err != nil || pong != "PONG" {
		return nil, err
		// panic(err)
	}

	log.Println("[Redis] open db successed.")
	return client, nil
}

func getDBConfig(dbName string) data.DBSetting {
	settings := data.DBSetting{}

	viper.UnmarshalKey("database."+dbName, &settings)
	// default
	if settings.Hostname == "" {
		settings.Hostname = "localhost"
	}
	if settings.Port == "" {
		switch dbName {
		case "mongo":
			settings.Port = "27017"
		case "mysql":
			settings.Port = "3306"
		case "redis":
			settings.Port = "6379"
		default:
		}
	}
	if settings.User == "" {
		settings.User = "root"
	}
	if settings.Password == "" {
		settings.Password = "root"
	}

	return settings
}
