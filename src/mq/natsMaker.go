package mq

import (
	"fmt"
	"log"
	"time"

	"github.com/nats-io/nats.go"
)

type natsmq uint

var NatsMQ natsmq

var Natsconn *nats.Conn

func (n *natsmq) Connect() {
	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		log.Fatal(err)
	}
	Natsconn = nc

	n.BroadcastEveryHour()
}

func (n *natsmq) BroadcastEveryHour() {
	now := time.Now()

	// the duration until the next hour
	nextHour := now.Truncate(time.Hour).Add(time.Hour)
	duration := nextHour.Sub(now)

	timer := time.NewTimer(duration)

	quit := make(chan bool)

	go func() {
		for {
			select {
			case <-timer.C:
				msg := []byte(fmt.Sprintf("it's now %v", time.Now().Format("3:04 PM")))
				log.Println(string(msg))
				// Broadcast the message to all subscribers
				err := Natsconn.Publish("main", msg)
				if err != nil {
					log.Fatal(err)
				}
				timer.Reset(time.Hour)
			case <-quit:
				Natsconn.Close()
				return
			}
		}
	}()
}
