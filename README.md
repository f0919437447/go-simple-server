# Simple Server APP

## Runs database service
docker compose up -d

## API TEST
GET: http://localhost:8080/users
GET: http://localhost:8080/send
POST: http://localhost:8080/login

## NATS TEST
### test by an API that as an publisher by sending the message
GET: http://localhost:8080/trigger/:message