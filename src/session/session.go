package session

import (
	"context"
	"main/src/database"
	"main/src/utils"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/securecookie"
	"github.com/vmihailenco/msgpack"
)

type GenericContext interface {
	*context.Context | *gin.Context
	Value(key any) any
}

type Expire struct {
	session time.Duration
	cookie  time.Duration
}

type Session struct {
	Name     string
	Id       string
	Values   map[any]any
	CreateAt time.Duration
	ExpireAt time.Duration

	mu        sync.RWMutex         `msgpack:"-"`
	ctx       *gin.Context         `msgpack:"-"`
	ExpireTTL Expire               `msgpack:"-"`
	Submit    bool                 `msgpack:"-"`
	Codecs    []securecookie.Codec `msgpack:"-"`
}

// GetInstence gets session instence from context
func GetInstence(ctx context.Context) *Session {
	if val := ctx.Value(utils.SessionKey); val != nil {
		return val.(*Session)
	}

	return nil
}

func NewSession(ctx *gin.Context, name string, expire Expire) *Session {
	createAt := time.Duration(time.Now().UnixNano())
	session := &Session{
		Name: name,
		// Id:        utils.RandStringBytesMaskImprSrcSB(32),
		Values:    make(map[any]any),
		CreateAt:  createAt,
		ExpireAt:  createAt + expire.session,
		ExpireTTL: expire,
		ctx:       ctx,
		Codecs:    securecookie.CodecsFromPairs([]byte(utils.SecretKey)),
	}
	return session
}

func (s *Session) getInstenceFromRedis() {
	if s.Id == "" {
		panic(utils.ErrLostSessionId)
	}

	redis := database.Redis
	if b, err := redis.Get(context.Background(), utils.SessionPrefixRedisKey+s.Id).Bytes(); err == nil {
		var session Session
		if err := msgpack.Unmarshal(b, &session); err != nil {
			panic(err)
		}
		s.Values = session.Values
	} else {
		// new
		s.Submit = true
	}
}

// HandleCookie get session key from cookie and get instence from redis by key
// then set back to cookie
func (s *Session) HandleCookie() {
	exists := s.getCookieKey()

	// it's a new key
	if !exists {
		s.setCookieKey(true)
	}
}

// getCookieKey returns whether the key of session exists, and if exists find instence from redis
//
// *** If you want to get session key that stored in cookie and then fetch from redis call HandleCookie, please
func (s *Session) getCookieKey() bool {
	var key string
	if encrypt, err := s.ctx.Cookie(utils.SessionKey); err == nil {
		if err := securecookie.DecodeMulti(s.Name, encrypt, &key, s.Codecs...); err == nil {
			panic(err)
		} else {
			s.Id = key
			s.getInstenceFromRedis()
			return true
		}
	}

	// new key
	s.Id = utils.RandStringBytesMaskImprSrcSB(32)
	return false
}

// GetCookieKey returns whether the key of session has been set
//
// *** If you want to get session key that stored in cookie and then fetch from redis call HandleCookie, please
func (s *Session) setCookieKey(force bool) bool {
	if force || s.ctx.Writer.Header().Get(utils.CookieHeader) == "" {
		if encrypt, err := securecookie.EncodeMulti(s.Name, s.Id, s.Codecs...); err != nil {
			panic(err)
		} else {
			s.ctx.SetCookie(s.Name, encrypt, int(s.ExpireTTL.cookie.Seconds()), "/", "", true, false)
			return true
		}
	}
	return false
}

// Save updates session instence into redis
func (s *Session) Save() {
	redis := database.Redis

	if s.Submit {
		if b, err := msgpack.Marshal(s); err != nil {
			panic(err)
		} else {
			if err := redis.SetEX(context.Background(), utils.SessionPrefixRedisKey+s.Id, b, s.ExpireTTL.session).Err(); err != nil {
				panic(err)
			}
		}
	} else {
		if _, err := redis.Expire(context.Background(), utils.SessionPrefixRedisKey+s.Id, s.ExpireTTL.session).Result(); err != nil {
			panic(err)
		}
	}

	s.setCookieKey(true)
}

func (s *Session) GetValue(key any) any {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return s.Values[key]
}

func (s *Session) SetValue(key, val any) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.Values[key] = val
	s.Submit = true
}

func (s *Session) DelValue(key any) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.Values, key)
	s.Submit = true
}
