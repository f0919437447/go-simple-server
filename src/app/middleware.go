package app

import (
	"runtime"

	"github.com/gin-gonic/gin"
)

// ConcurrencyLimit limits how many request can be processed in the same time based on `max` value
func ConcurrencyLimit(max int) gin.HandlerFunc {
	// any value below zero will be considered as it meant to be disabled
	if max < 0 {
		return func(ctx *gin.Context) { ctx.Next() }
	}
	if max == 0 {
		max = runtime.NumCPU()
	}
	sem := make(chan struct{}, max)
	acquire := func() { sem <- struct{}{} }
	release := func() { <-sem }

	return func(ctx *gin.Context) {
		acquire()
		defer release()
		ctx.Next()
	}
}
