﻿package controller

import (
	"io/ioutil"
	"log"
	"main/src/data"
	"main/src/database"
	"main/src/mq"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
	"github.com/vmihailenco/msgpack"
)

type user uint

var User user = 2

func (u user) Route(server *gin.RouterGroup) {
	server.GET(data.HTTP_USERS, u.users)
	server.POST(data.HTTP_LOGIN, u.login)
	server.GET(data.HTTP_SEND, u.send)

	server.GET(data.HTTP_TRIGGER_TEST_MQ, u.triggerTest)
}

func (u user) NatsRoute(nc *nats.Conn) {
	_, err := nc.Subscribe("test", func(msg *nats.Msg) {
		log.Printf("Received message: %s", msg.Data)
	})
	if err != nil {
		log.Fatal(err)
	}
}

func (u user) triggerTest(ctx *gin.Context) {
	msg := ctx.Param("message")
	if err := mq.Natsconn.Publish("test", []byte(msg)); err != nil {
		log.Fatal(err)
	}
	ctx.JSON(http.StatusOK, msg)
}

func (u user) users(ctx *gin.Context) {
	users := make([]data.User, len(data.Users))
	for i, u := range data.Users {
		users[i] = data.User{ID: u.ID, Name: "****", Passwd: "****"}
	}
	ctx.JSON(http.StatusOK, users)
}

func (u user) login(ctx *gin.Context) {
	body := ctx.Request.Body

	b, err := ioutil.ReadAll(body)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, nil)
	}

	// client must pass name and password
	user := data.User{}
	if err = msgpack.Unmarshal(b, &user); err != nil || user.Name == "" || user.Passwd == "" {
		ctx.JSON(http.StatusInternalServerError, nil)
	}

	// simple verify
	for _, u := range data.Users {
		if user.Name == u.Name && user.Passwd == u.Passwd {
			ctx.JSON(http.StatusOK, data.Users)
		}
	}

	ctx.JSON(http.StatusBadRequest, nil)

	// simple login success
	database.Redis.Set(ctx, user.Name, user, time.Hour)
}

func (u user) send(ctx *gin.Context) {
	// ...
	ctx.JSON(http.StatusOK, "OK")
}
