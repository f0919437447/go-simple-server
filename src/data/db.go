package data

type DBSetting struct {
	Hostname string
	Port     string
	User     string
	Password string
}
