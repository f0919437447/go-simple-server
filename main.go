﻿package main

import (
	"main/src/app"
	"main/src/database"
	"main/src/mq"
	"os"
	"os/signal"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// go get -u github.com/gin-gonic/gin
// go get -u gorm.io/gorm

var server *gin.Engine

func main() {
	err := loadConfig("config")
	if err != nil {
		panic(err)
	}

	// connect to db, you can disable db use by config
	database.DBMaker.Connect()
	mq.NatsMQ.Connect()
	// run app
	app.Server.Run(server, mq.Natsconn)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
}

func loadConfig(filename string) error {
	viper.SetConfigName(filename)
	viper.AddConfigPath(".")
	viper.SetConfigType("yaml")

	return viper.ReadInConfig()
}
