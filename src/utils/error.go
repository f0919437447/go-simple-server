package utils

import "errors"

var (
	ErrLostSessionId = errors.New("lost session Id")
)
