package auth

import (
	"main/src/data"
	"strings"

	"github.com/gin-gonic/gin"
)

func CheckContext(ctx *gin.Context) {
	path := ctx.Request.URL.Path
	query := ctx.Request.URL.RawQuery
	query = strings.Replace(query, "?", "", -1)

	// check path
	if _, ok := data.ValidURLPath[path]; !ok {
		return
	}

	// check parameters
	queryParams := strings.Split(query, "&")
	if queryParams[0] != "" {
		for _, param := range queryParams {

			key := strings.Split(param, "=")
			if _, ok := data.ValidURLQuery[path][key[0]]; !ok {
				return
			}
		}
	}

	// ...verify
}
