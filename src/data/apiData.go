﻿package data

type User struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Passwd string `json:"passwd"`
}

const (
	API_ROOT   = "/api"
	HTTP_USERS = "/users"
	HTTP_LOGIN = "/login"
	HTTP_SEND  = "/send"

	HTTP_TRIGGER_TEST_MQ = "/trigger/:message"
)

var ValidURLPath = map[string]bool{
	HTTP_USERS: true,
	HTTP_LOGIN: true,
	HTTP_SEND:  true,
}

var ValidURLQuery = map[string]map[string]bool{
	HTTP_USERS: {"name": true, "passwd": true},
	HTTP_LOGIN: {"name": true, "passwd": true},
	HTTP_SEND:  {"from": true, "to": true},
}

var Users = []User{
	{ID: 0, Name: "root", Passwd: "root"},
	{ID: 1, Name: "user1", Passwd: "user1"},
	{ID: 2, Name: "user2", Passwd: "user2"},
}
